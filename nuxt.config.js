export default {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
   
    title: "Brasil Paralelo",
    htmlAttrs: {
        lang: "pt-BR"
    },
    meta: [{
        charset: "utf-8"
    }, {
        name: "viewport",
        content: "width=device-width, initial-scale=1"
    }, {
        hid: "og:site_name",
        name: "og:site_name",
        content: "Brasil Paralelo"
    }, { 
        hid: "og:url",
        name: 'og:url',
        content: 'https://brasilparalelo.com.br' 
    }, {
        hid: "og:image",
        name: 'og:image',
        content: 'https://brasilparalelo.com.br/_nuxt/img/4638014.jpg' 
    }, {
        name: "og:locale",
        content: "pt_BR"
    }],
    link: [{
        rel: "icon",
        type: "image/x-icon",
        href: "/favicon.ico"
    }],
    script: [
        /*{
            src: '/viral.js'
        },*/

    ],
    link: [
      { rel: 'stylesheet', href: 'https://use.typekit.net/eup5jcd.css' }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: '~/plugins/vue-carousel', mode: 'client' },
    { src: '~/plugins/youtube', mode: 'client' },
    { src: '~/plugins/countdown', mode: 'client' },
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    //'@nuxtjs/eslint-module'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: ['@nuxtjs/style-resources'],
  styleResources: {
    scss: './assets/sass/*.scss'
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  }
}
