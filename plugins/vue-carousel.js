import Vue from 'vue';
import VueCarousel from 'vue-carousel';
import VueVideoSection from 'vue-video-section'

Vue.use(VueCarousel);
Vue.component('vue-video-section', VueVideoSection)